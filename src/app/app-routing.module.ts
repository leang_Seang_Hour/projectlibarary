import { CostumerviewComponent } from './main-nav/costumer/costumerview/costumerview.component';
import { CostumercreateComponent } from './main-nav/costumer/costumercreate/costumercreate.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', component: MainNavComponent, children: [
    {path: 'createCustomer', component: CostumercreateComponent},
    {path: 'viewCustomer', component: CostumerviewComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
