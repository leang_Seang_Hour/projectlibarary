import { Injectable } from '@angular/core';
import { Costumer } from './costumer';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CostumerService {
  URL = 'http://localhost:3000';
  constructor( private httpclient : HttpClient) { }

  getCostumer():Observable<Costumer[]>
  {
    return this.httpclient.get<Costumer[]>(`${this.URL}`);
  }
  addCostumer(costumer: Costumer):Observable<Costumer>
  {
    console.log(costumer)
    return this.httpclient.post<Costumer>(`${this.URL}`,costumer);
  }
}
