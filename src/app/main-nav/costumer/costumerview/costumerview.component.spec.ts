import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostumerviewComponent } from './costumerview.component';

describe('CostumerviewComponent', () => {
  let component: CostumerviewComponent;
  let fixture: ComponentFixture<CostumerviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostumerviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostumerviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
