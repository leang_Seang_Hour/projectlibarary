import { Component, OnInit } from '@angular/core';
import {Costumer} from '../costumer';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {CostumerService} from '../costumer.service';
@Component({
  selector: 'app-costumercreate',
  templateUrl: './costumercreate.component.html',
  styleUrls: ['./costumercreate.component.css']
})


export class CostumercreateComponent implements OnInit {
  selected = 'option2';
  costumer: Costumer[];
  constructor(private costumerservice:CostumerService ) {}
  
  formgroup=new FormGroup({
    firstname : new FormControl('',Validators.required),
    lastname : new FormControl('',Validators.required),
    gender : new FormControl('',Validators.required),
    telephone : new FormControl('',Validators.required),
    address : new FormControl('',Validators.required),
    email: new FormControl('',Validators.required),
    password: new FormControl('',Validators.required),
    cpassword: new FormControl('',Validators.required)
  });

  Submit(formgroup)
  {
    console.log(formgroup)
      this.formgroup=formgroup;
      if(this.formgroup.get('firstname').value !='' && this.formgroup.get('lastname').value !='' && this.formgroup.get('gender').value !='' && this.formgroup.get('telephone').value != '' && this.formgroup.get('address').value != ''&& this.formgroup.get('email').value != ''&& this.formgroup.get('password').value != '' && this.formgroup.get('cpassword').value != '')
      {
        this.costumerservice.addCostumer(this.formgroup.value).subscribe((res)=>{
          console.log(res);
        });
        console.log("SUCESS");
      }
      else
      {
        alert("Please Check Your input Again");
      }
  }

  ngOnInit(): void {
  }
}
