import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostumercreateComponent } from './costumercreate.component';

describe('CostumercreateComponent', () => {
  let component: CostumercreateComponent;
  let fixture: ComponentFixture<CostumercreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostumercreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostumercreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
